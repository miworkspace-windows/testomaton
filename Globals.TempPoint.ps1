﻿#--------------------------------------------
# Declare Global Variables and Functions here
#--------------------------------------------

#Sample function that provides the location of the script
function Get-ScriptDirectory
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($null -ne $hostinvocation)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}

#Sample variable that provides the location of the script
[string]$ScriptDirectory = Get-ScriptDirectory

function Get-AppEnforceResults
{
	param ([string]$ApplicationInQuestion)
	#log -text "Checking for failed application installs"             #log -text "Checking for failed application installs since $starttime"
	$applog = Get-MultipleCCMLogs -BaseLogName "AppEnforce" -StartTime "11/11/2015" -EndTime "Now" -SearchPath "C:\Windows\CCM\Logs"
	#$applog = Get-Content C:\Windows\ccm\logs\AppEnforce.log
	$AppsEnforced = @()
	$AppStatus = @{ }
	$AppErrors = "\+{3}\sApplication not discovered", "considered an execution failure", "A supported AppV client is not installed", "failed with error code"
	[int]$intFailed = 0
	
	#AppEnforce completion status message choices:
	# ++++++ App enforcement completed - may or may not be a failure (see previous line if not Failed to enforce app)
	# Matched exit code 3010 to a PendingSoftReboot entry in exit codes table = Success
	# \+{3}\sDiscovered application = Success
	# \+{3}\sDiscovered MSI application = Success
	# \+{3}\sApplication not discovered = Failure
	# \+{3}\sApplication not discovered with script detection = Failure
	# Unmatched exit code (1) is considered an execution failure = Failure
	# Unmatched exit code (13) is considered an execution failure = Failure                        
	# A supported AppV client is not installed = Failure
	# ++++++ Failed to enforce app - as strline = Failure         Previous line also says "Method CommenceEnforcement failed with error code 87D0031D	AppEnforce	9/27/2016 9:17:42 AM	1144 (0x0478)                        
	# Matched exit code 3010 to a PendingSoftReboot entry in exit codes table = Success pending reboot
	# Waiting for user logoff = Pending (Not a failure)
	
	[int]$intEntries = $applog.Count
	If ($intEntries -gt 0)
	{
		$currentApp = ""
		$InstallCycle = $false
		$lastline = ""
		$strline = ""
		#$intEntries = $intEntries - 1 - don't need this because it's Less Than $intEntries - if it were LEss Than or Equal, we'd need this
		for ($i = 0; $i -lt $intEntries; $i++)
		{
			[string]$strLine = $applog[$i]
			#Extract date and time and then compare to start time
			$tempDate = ([regex]::Match($strline, "(?<=date=`").*?(?=\`")")).value
			If ($tempDate)
			{
				$tempTime = ([regex]::Match($strline, "(?<=time=`").*?(?=\`")")).value
				If ($tempTime)
				{
					$entryDT = $tempDate + " " + $tempTime
					[datetime]$thisdate = [datetime]$entryDT
					If ($thisdate -ge $starttime)
					{
						#This value is part of the eval process
						If ($strLine -match "\+{3}\sStarting Install enforcement")
						{
							#This is the beginning of a new App Enforcement - so let's set it up
							$InstallCycle = $true
							$currentApp = ([regex]::Match($strline, "(?<=\`").*?(?=\`")")).value
						}
						Else
						{
							If ($strLine -match "\+{6}")
							{
								If ($InstallCycle -and ($currentApp -ne "" -and $currentApp -ne $null))
								{
									#Process as the conclusion - otherwise it may be an impartial record and therefore let's skip it
									#Work through each app error message to see if this app matches one of them indicating a failure
									If ($strline -match "Failed to enforce app")
									{
										#This is a big indication of failure
										#Check if it's in the hashtable and failed list - if not add, if so, update it
										$KeyExists = $AppStatus.ContainsKey($currentApp)
										If ($KeyExists)
										{
											$AppStatus.Item($currentApp) = "Failed enforcement"
										}
										Else
										{
											$AppStatus.Add($currentApp, "Failed enforcement")
										}
									}
									Else
									{
										$MatchedError = $false
										$TheError = ""
										foreach ($AppError in $AppErrors)
										{
											If ($lastline -match $AppError)
											{
												#We have an error and this is a fail
												$MatchedError = $true
												$TheError = $AppError
												
												#Check if it's in the hashtable and failed list - if not add, if so, update it
												$Reason = ""
												If ($AppError -match "not discovered")
												{
													$Reason = "Failed - not found after install"
												}
												Else
												{
													If ($AppError -match "error code")
													{
														$Reason = "Failed with an error"
													}
													Else
													{
														If ($AppError -match "execution failure")
														{
															$Reason = "Failed with unknown exit code"
														}
														Else
														{
															$Reason = "Failed because no AppV client"
														}
													}
												}
												$KeyExists = $AppStatus.ContainsKey($currentApp)
												If ($KeyExists)
												{
													$AppStatus.Item($currentApp) = $Reason
												}
												Else
												{
													$AppStatus.Add($currentApp, $Reason)
												}
											}
										}
										If ($MatchedError -eq $false)
										{
											#Check for waiting for user which is pending
											If ($lastline -match "aiting for user logoff")
											{
												#Not failures - just waiting
												$KeyExists = $AppStatus.ContainsKey($currentApp)
												If ($KeyExists)
												{
													$AppStatus.Item($currentApp) = "Incomplete"
												}
												Else
												{
													$AppStatus.Add($currentApp, "Incomplete")
												}
											}
											Else
											{
												If ($lastline -match "Discovered application" -or $lastline -match "Discovered MSI application" -or $lastline -match "Reboot")
												{
													#This is success
													$KeyExists = $AppStatus.ContainsKey($currentApp)
													If ($KeyExists)
													{
														$AppStatus.Item($currentApp) = "Success"
													}
													Else
													{
														$AppStatus.Add($currentApp, "Success")
													}
												}
												Else
												{
													#This is unknown so let's log it
													$tempString = $tempString.Replace("<![LOG[", "")
													$tempString = $tempString.Replace("`+", "")
													[string]$tempString = CleanString -StringToClean $AppError
													If ($tempString.length -gt 30)
													{
														$tempString = $tempString.Substring(0, 30)
													}
													log -text "Unknown Enforce Status found for app $($currentApp): $($tempString)"
													$KeyExists = $AppStatus.ContainsKey($currentApp)
													If ($KeyExists)
													{
														$AppStatus.Item($currentApp) = "Unknown"
													}
													Else
													{
														$AppStatus.Add($currentApp, "Unknown")
													}
												}
											}
										}
									}
								}
								Else
								{
									#Skipping this app because it appears to be a partial entry
								}
								#Reseting the data because we've completed the enforcement cycle for this application
								$currentApp = ""
								$InstallCycle = $false
							}
						}
					}
				}
			}
			[string]$lastline = $strline
		}
	}
	#This is success
	#$KeyExists = $AppStatus.ContainsKey($ApplicationInQuestion)
	$KeyExists = $AppStatus.Keys -match $applicationinQuestion
	If ($KeyExists)
	{
		$testtemp = $AppStatus.Item("$KeyExists")
	}
	Else
	{
		$testtemp = "No results found"
	}
	Return $testtemp
	Else
	{
		Return "No results found"
	}
}

Function IsProcessRunning
{
	param ([string]$ProcessName)
	
	$FoundItRunning = $False
	$proc = Get-Process
	$ProcessName = $ProcessName.ToUpper()
	foreach ($runningapp in $proc)
	{
		$Testvalue = $runningapp.ProcessName
		$Testvalue = $Testvalue.ToUpper()
		If ($Testvalue -eq $ProcessName)
		{
			$FoundItRunning = $true
		}
	}
	If ($FoundItRunning)
	{
		return $FoundItRunning
	}
	Else
	{
		return $false
	}
}

function RunCommandJob ()
{
	param ($CommandString = $null,
		$SecondsToWait = $null)
	
	If ($CommandString)
	{
		$Secondscounter = 0
		$theJobToRun = Start-Job -ScriptBlock {
			param ([string]$cmdstring)
			cd $env:TEMP
			$outcome = cmd /c $cmdstring | Out-String
			Write-Output "$outcome"
		} -ArgumentList ($CommandString)
		Do
		{
			$Secondscounter += 1
			Start-Sleep -Seconds 1
		}
		Until ($theJobToRun.state -eq "Completed" -or $Secondscounter -ge $SecondsToWait)
		$ResultsOfThisJob = Receive-Job $theJobToRun
		return $ResultsOfThisJob
		#return $outcome
	}
	else
	{
		return $null
	}
}

Function FileWatcher ([string]$TheFilePath, [int64]$MinutesDone, [int64]$MaxMinutesAgoChanged, [int64]$MinutesToTry)
{
	$ReadyToAdvance = $false
	$MaxMinutesAgoChanged = 0 - [int]$MaxMinutesAgoChanged
	$MinutesDone = 0 - $MinutesDone
	[datetime]$timeBegin = (Get-Date).AddMinutes($MaxMinutesAgoChanged)
	for ($j = 0; $j -lt $MinutesToTry; $j++)
	{
		If (Test-Path -Path $TheFilePath)
		{
			[datetime]$LastDate = [datetime](Get-ItemProperty -Path $TheFilePath -Name LastWriteTime).lastwritetime
		}
		Else
		{
			[datetime]$LastDate = (Get-Date)
		}
		
		[datetime]$timeEnd = (Get-Date).AddMinutes($MinutesDone)
		#Now Evaluate success - More than $minutesDone but less than MaxMinutesAgo		
		If (([datetime]$LastDate -ge [datetime]$timeBegin) -and ([datetime]$LastDate -le [datetime]$timeEnd))
		{
			#We have a recently completed file
			$ReadyToAdvance = $true
			[int]$j = [int]$MinutesToTry
		}
		Else
		{
			Start-Sleep -Seconds 60
			[System.Windows.Forms.SendKeys]::SendWait("{F15}")
		}		
	}
	return $ReadyToAdvance
}

function Get-MultipleCCMLogs ([string]$BaseLogName, [datetime]$StartTime, [string]$EndTime, [string]$SearchPath)
{
	#This is the complex retrieval of all logs starting with this name to acquire archived logs too	
	$AllResults = @()
	$AllLogs = gci $SearchPath | ?{ $_.name.startswith("$BaseLogName", "currentcultureignorecase") }
	If ($AllLogs.count -gt 1)
	{
		$ProperOrder = $AllLogs | Sort-Object $_.Name -Descending
		$LogCount = ($ProperOrder).Count
		for ($i = 0; $i -lt $LogCount; $i++)
		{
			$AllResults += Get-Content $ProperOrder[$i].FullName
		}
	}
	Else
	{
		$AllResults = Get-Content $AllLogs.FullName
	}
	
	#Now that we have all data in order, let's process the contents and only return what is relevant
	If ($EndTime -eq "Now")
	{
		$EndTime = (Get-Date).tostring()
	}
	$FinalResults = @()
	$entryDT = "1/1/1900"
	$PreviousDate = "1/1/1900"
	$PreviousTime = "23:00:00"
	[int]$LineCount = ($AllResults).Count
	for ($i = 0; $i -lt $LineCount; $i++)
	{
		$logline = $AllResults[$i]
		#Extract date and time out of this line
		#Extract date and time and then compare to start time
		$tempDate = ([regex]::Match($logline, "(?<=date=`").*?(?=\`")")).value
		If ($tempDate)
		{
			$PreviousDate = $tempDate
		}
		Else
		{
			$tempDate = $PreviousDate
		}
		[string]$tempTime = ([regex]::Match($logline, "(?<=time=`").*?(?=\`")")).value
		If ($tempTime)
		{
			If ($tempTime.IndexOf(".") -gt 0)
			{
				$tempInt = $tempTime.IndexOf(".")
				$tempInt = $tempInt
				$tempTime = $tempTime.Substring(0, ($tempInt))
				$tempTime = $tempTime.Replace(".", "")
			}
			$PreviousTime = $tempTime
		}
		Else
		{
			$tempTime = $PreviousTime
		}
		$entryDT = $tempDate + " " + $tempTime
		$entryDT = Get-date($entryDT) -format g
		
		If ([datetime]$entryDT -ge [datetime]$StartTime -and [datetime]$entryDT -le [datetime]$EndTime)
		{
			#We have a keeper
			$FinalResults += $logline
		}
	}
	return $FinalResults
}

Function FileJustChanged ([string]$TheFilePath, $ChangedAfter, [int]$MinutesToTry)
{
	$ReadyToAdvance = $false
	$TimesUp = (Get-Date).AddMinutes($MinutesToTry)
	for ([int]$j = 0; [int]$j -lt [int]$MinutesToTry; $j++)
	{
		If (Test-Path -Path $TheFilePath)
		{
			[datetime]$LastDate = [datetime](Get-ItemProperty -Path $TheFilePath -Name LastWriteTime).lastwritetime
		}
		Else
		{
			[datetime]$LastDate = "11/11/2011 11:11:11 PM"
		}
		
		#Now Evaluate if changed within timeframe desired
		If ([datetime]$LastDate -ge [datetime]$ChangedAfter)
		{
			#We have a recently completed file
			$ReadyToAdvance = $true
			[int]$j = [int]$MinutesToTry
		}
		Else
		{
			[System.Windows.Forms.SendKeys]::SendWait("{F15}")
			Start-Sleep -Seconds 60
		}
	}
	return $ReadyToAdvance
}

function CompareObjects
{
	param (
		[Parameter(Mandatory = $true)]
		[object]$Reference,
		[Parameter(Mandatory = $true)]
		[object]$Difference,
		[Parameter(Mandatory = $true)]
		[string]$MatchString
	)
	
	$IsDifferent = $false
	$StringMatched = $null
	
	#Let's fix the lists to make sure they are truly clean and sorted
	[System.Collections.ArrayList]$arrRef = new-object system.collections.arraylist
	[System.Collections.ArrayList]$arrDiff = new-object system.collections.arraylist
	foreach ($line in $Reference)
	{
		[string]$test = $line.Name
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line.ProductName
		}
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line.FullName
		}
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line
		}
		[System.Collections.ArrayList]$arrRef.Add($test)
	}
	foreach ($line in $Difference)
	{
		[string]$test = $line.Name
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line.ProductName
		}
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line.FullName
		}
		If ($test -eq "" -or $test -eq $null)
		{
			[string]$test = $line
		}
		[System.Collections.ArrayList]$arrDiff.Add($test)
	}
	$arrRef = $arrRef | sort-object
	$arrDiff = $arrDiff | sort-object
	
	#$TheResponse = (compare-object $arrBefore $arrAfter).InputObject
	
	$TheDiff = compare-object -ReferenceObject $arrRef -DifferenceObject $arrDiff
	[int]$NumberOfDifferences = ($TheDiff | measure).count
	If ($NumberOfDifferences -gt 0)
	{
		$IsDifferent = $true
		If ($NumberOfDifferences -eq 1)
		{
			If ($MatchString -match $TheDiff.InputObject.Name)
			{
				$StringMatched = $true
			}
			Else
			{
				$StringMatched = $false
			}
		}
		Else
		{
			#Need to sort through all of them to find one that matches
			foreach ($oneDiff in $TheDiff)
			{
				If ($oneDiff.InputObject.Name -match $MatchString)
				{
					$StringMatched = $true
				}
			}
			If ($StringMatched -eq $null) { $StringMatched = $false }
		}
	}
	Else
	{
		$IsDifferent = $false
	}
	
	$MyResponse = @{ }
	$MyResponse.IsDifferent = $IsDifferent
	$MyResponse.IsAMatch = $StringMatched
	$MyResponse.NumberofDifferences = $NumberOfDifferences
	$MyResponse.Differences = $TheDiff
	
	return $MyResponse
}

function send-hipchat-v2
{
	param ([string]$message,
		[string]$roomid,
		$color = "yellow")
	
	#Example: send-hipchat-v2 -message "test" -roomid "387119" -color purple
	$uri = "https://api.hipchat.com/v2/room/$roomid/notification?auth_token=YOURTOKENHERE"
	$postParams = "message=$message&color=$color"
	$PostStr = [System.Text.Encoding]::UTF8.GetBytes($Postparams)
	$webrequest = [System.Net.WebRequest]::Create($uri)
	$webrequest.ContentType = "application/x-www-form-urlencoded"
	$webrequest.Method = "POST"
	$webrequest.ContentLength = $PostStr.Length
	$webRequest.ServicePoint.Expect100Continue = $false
	
	$requestStream = $webRequest.GetRequestStream()
	$requestStream.Write($PostStr, 0, $PostStr.length)
	$requestStream.Close()
	
	#Response
	[System.Net.WebResponse]$resp = $webRequest.GetResponse();
	$rs = $resp.GetResponseStream();
	[System.IO.StreamReader]$sr = New-Object System.IO.StreamReader -argumentList $rs;
	[string]$results = $sr.ReadToEnd();
	return $results;
}

function invoke-postmethod($url, $RequestDigest, $etag, $XHTTPMethod)
{
	$request = [System.Net.WebRequest]::Create($Url)
	$request.Credentials = $global:spoCred
	$request.Accept = "application/json;odata=verbose"
	$request.ContentType = "application/json;odata=verbose"
	$request.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f")
	$request.Method = 'POST'
	if (![string]::IsNullOrEmpty($ETag))
	{
		$request.Headers.Add("If-Match", $ETag)
	}
	if (![string]::IsNullOrEmpty($RequestDigest))
	{
		$request.Headers.Add("X-RequestDigest", $RequestDigest)
	}
	if (![string]::IsNullOrEmpty($XHTTPMethod))
	{
		$request.Headers.Add("X-HTTP-Method", $XHTTPMethod.ToUpper())
	}
	if ($Metadata -is [string] -and ![string]::IsNullOrEmpty($Metadata))
	{
		$body = [System.Text.Encoding]::UTF8.GetBytes($Metadata)
		$request.ContentLength = $body.Length
		$stream = $request.GetRequestStream()
		$stream.Write($body, 0, $body.Length)
	}
	else
	{
		$request.ContentLength = 0
	}
	$response = $request.GetResponse()
	$streamReader = New-Object System.IO.StreamReader $response.GetResponseStream()
	$data = $streamReader.ReadToEnd()
	return $data
}

function modify-rob
{
	param ([string]$id,
		[string]$deployment_status = $null,
		[string]$testing_notes = $null)
	
	$username = "m-sharepoint"
	$password = "P@ssword1234@#"
	$secstr = New-Object -TypeName System.Security.SecureString
	$password.ToCharArray() | ForEach-Object { $secstr.AppendChar($_) }
	$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $secstr
	$global:spoCred = $credential
	
	$properties = @()
	
	$objectt = "" | select Title, Value
	$objectt.Title = "Deployment_x0020_Status"
	$objectt.value = $deployment_status
	if (![string]::IsNullOrEmpty($deployment_status)) { $properties += $objectt }
	
	$objectt = "" | select Title, Value
	$objectt.Title = "Testing_x0020_Notes"
	$objectt.value = $testing_notes
	if (![string]::IsNullOrEmpty($testing_notes)) { $properties += $objectt }
	
	$url = "https://sharepoint.umich.edu/bf/its/projects/euc/_api/Web/Lists(guid'0E1E007B-CA8F-44D4-A499-7B8A463E627D')/items"
	$itemtype = "SP.Data.PackagesListItem"
	$metadata = "{ '__metadata': { 'type': '$itemType' }"
	foreach ($property in $properties)
	{
		$metadata = ($metadata + ", '" + $property.title + "': '" + $property.Value + "'")
	}
	$metadata += '}'
	$modifyurl = ($url + '(' + $id + ')')
	write-host 'here'
	$digest = (invoke-postmethod -URL 'https://sharepoint.umich.edu/bf/its/projects/euc/_api/contextinfo' -Method Post)
	$digest = $digest -replace '.*FormDigestValue":"' -replace '".*'
	write-host $digest
	write-host $metadata
	write-host $modifyurl
	invoke-postmethod -url $modifyurl -Method Post -XHTTPMethod Merge -Metadata $metadata -RequestDigest $digest -etag "*"
}
# ex: modify-rob -id 10169 -deployment_status "Ready For Testing"

function ZipThisFile ([string]$zipfullname, [string]$filetozippath)
{
	$outcome = & "C:\Program Files\7-Zip\7z.exe" -tzip a $zipfullname $filetozippath
}
